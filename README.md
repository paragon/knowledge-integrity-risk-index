# knowledge-integrity-risk-index

This repo contains notebooks created to compute metrics of the [Knowledge Integrity Risk Observatory  (v2)](https://superset.wikimedia.org/superset/dashboard/riskobservatory2), based on revision scores predicted with the [language-agnostic revert risk model](https://meta.wikimedia.org/wiki/Machine_learning_models/Proposed/Language-agnostic_revert_risk). More information about the risk observatory can be found at the [page on Meta](https://meta.wikimedia.org/wiki/Research:Wikipedia_Knowledge_Integrity_Risk_Observatory).

## Repository structure
```
The structure of the repository is outlined below.
.
├── data: Contains files (csv, parquet) resulting from the notebooks
├── figures: Contains plots (png, pdf) resulting from the notebooks analyzing thresholds
├── notebooks:
│   └── community-capacity.ipynb: notebook for computing metrics on admin capacity
│   └── content-quality-boxplots.ipynb: notebook for computing article quality data to expose in the dashboard with a boxplot 
│   └── content-quality.ipynb: notebook for computing metrics on article quality
│   └── content-reverts-0-data-collection-20212022.ipynb: notebook for computing revert risk scores
│   └── content-reverts-1-analyze-thresholds-all.ipynb: notebook for analyzing thresholds of revert risk scores
│   └── content-reverts-1-analyze-thresholds-anonymous.ipynb: notebook for analyzing thresholds of revert risk scores distinguishing IP edits
│   └── content-reverts-1-analyze-thresholds-bot.ipynb: notebook for analyzing thresholds of revert risk scores distinguishing bot edits
│   └── content-reverts-1-analyze-thresholds-self.ipynb: notebook for analyzing thresholds of revert risk scores distinguishing self-reverted edits
│   └── content-reverts-2-highrisk-thresholds-20212022.ipynb: notebook for computing metrics on high risk revisions (i.e. the threshold in which the model accuracy is maximum)
│   └── content-reverts-3-highrisk-editorspages-20212022.ipynb: notebook for aggregating high risk revision data by page and editor
│   └── utils: Contains a csv file used by the language-agnostic quality model and the pkl file of the language-agnostic revert risk model
└── README.md
```
## Authors and acknowledgment
Contributors: [@paragon](https://gitlab.wikimedia.org/paragon) with the support of [@mnz](https://gitlab.wikimedia.org/mnz)
